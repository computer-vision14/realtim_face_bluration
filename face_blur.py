import cv2
import numpy as np

def blur(img, k):
    h, w = img.shape[:2]
    kh, kw = h // k, w // k
    if kh % 2 == 0:
        kh -= 1
    if kw % 2 == 0:
        kw -= 1
    img = cv2.GaussianBlur(img, ksize=(kh, kw), sigmaX=0)
    return img

def pixelate_face(image, blocks=10):
    (h, w) = image.shape[:2]
    xSteps = np.linspace(0, w, blocks + 1, dtype="int")
    ySteps = np.linspace(0, h, blocks + 1, dtype="int")
    for i in range(1, len(ySteps)):
        for j in range(1, len(xSteps)):
            startX = xSteps[j - 1]
            startY = ySteps[i - 1]
            endX = xSteps[j]
            endY = ySteps[i]
            roi = image[startY:endY, startX:endX]
            (B, G, R) = [int(x) for x in cv2.mean(roi)[:3]]
            cv2.rectangle(image, (startX, startY), (endX, endY), (B, G, R), -1)
    return image

# Initialize video capture
cap = cv2.VideoCapture(0)
if not cap.isOpened():
    print("Error: Failed to open webcam.")
    exit()

# Load Haar cascade classifier for face detection
face_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')
if face_cascade.empty():
    print("Error: Failed to load Haar cascade classifier.")
    exit()

# Set parameters
pixelation_factor = 3    
blocks = 10

# Main loop
while True:
    ret, frame = cap.read()
    if not ret:
        print("Error: Failed to retrieve frame.")
        break
    
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Detect faces
    faces = face_cascade.detectMultiScale(gray, scaleFactor=1.3, minNeighbors=5)

    # Pixelate and blur each face
    for (x, y, w, h) in faces:
        face_region = frame[y:y+h, x:x+w]
        pixelated_face = blur(pixelate_face(face_region, blocks), pixelation_factor)
        frame[y:y+h, x:x+w] = pixelated_face

    # Display the modified frame
    cv2.imshow('Live', frame)

    # Check for 'Esc' key press to exit
    if cv2.waitKey(1) == 27:
        break

# Release video capture and close windows
cap.release()
cv2.destroyAllWindows()
