# Real-time Face Pixelation and Blurring

This Python script captures video from a webcam, detects faces in real-time, pixelates and blurs each detected face, and displays the modified video feed. It utilizes the OpenCV library for computer vision tasks.

## Features

- Real-time face detection using the Haar cascade classifier.
- Pixelation and blurring of detected faces.
- Adjustable parameters for pixelation factor and block size.
- Error handling for webcam and classifier loading failures.

## Requirements

- Python 3.x
- OpenCV (cv2) library

## Installation

1. Install Python 3.x from [python.org](https://www.python.org/downloads/).
2. Install OpenCV library using pip:

    ```
    pip install opencv-python
    ```

3. Download the script and the Haar cascade classifier file (`haarcascade_frontalface_default.xml`).

## Usage

1. Run the script using the following command:

    ```
    python face_pixelation.py
    ```

2. The webcam feed will open in a new window.
3. Detected faces will be pixelated and blurred in real-time.

## Customization

- Adjust the `pixelation_factor` and `blocks` parameters in the script to change the level of pixelation and block size, respectively.
- Experiment with other face detection methods or pixelation techniques for different effects.

## Troubleshooting

- If the script fails to open the webcam or load the classifier, ensure that the webcam is connected and the Haar cascade classifier file (`haarcascade_frontalface_default.xml`) is present in the correct directory.

## Credits

- This script was created by [Your Name].


